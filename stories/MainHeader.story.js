import React from 'react';
import MainHeader from '../app/components/MainHeader/index';

export default {
  title: 'MainHeader',
  Component: MainHeader,
};

export const ToStorybook = () => <MainHeader />;

ToStorybook.story = {
  name: 'to Storybook',
};
