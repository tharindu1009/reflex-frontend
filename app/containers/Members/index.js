/**
 *
 * Members
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectMembers from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import 'bootstrap/dist/css/bootstrap.min.css';
import SideNav from '../../components/SideNav';

export function Members() {
  useInjectReducer({ key: 'members', reducer });
  useInjectSaga({ key: 'members', saga });

  return (
    <div>
      {/* <Helmet>
        <title>Members</title>
        <meta name="description" content="Description of Members" />
      </Helmet>
      <FormattedMessage {...messages.header} /> */}
      <div>
        <SideNav />
      </div>
    </div>
  );
}

Members.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  members: makeSelectMembers(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Members);
