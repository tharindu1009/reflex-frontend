/**
 *
 * Dashboard
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectDashboard from './selectors';
import reducer from './reducer';
import saga from './saga';

import AppFooter from '../../components/AppFooter';

export function Dashboard() {
  useInjectReducer({ key: 'dashboard', reducer });
  useInjectSaga({ key: 'dashboard', saga });

  return <div class="content-page">
  <div class="content">

      <div class="container-fluid">
          
     
          <div class="row">
              <div class="col-12">
                  <div class="page-title-box">
                      <div class="page-title-right">
                          <form class="form-inline">
                              <div class="form-group">
                                  <div class="input-group input-group-sm">
                                      <input type="text" class="form-control border" id="dash-daterange"/>
                                      <div class="input-group-append">
                                          <span class="input-group-text bg-blue border-blue text-white">
                                              <i class="mdi mdi-calendar-range"></i>
                                          </span>
                                      </div>
                                  </div>
                              </div>
                              <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-2">
                                  <i class="mdi mdi-autorenew"></i>
                              </a>
                              <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-1">
                                  <i class="mdi mdi-filter-variant"></i>
                              </a>
                          </form>
                      </div>
                      <h4 class="page-title">Dashboard</h4>
                  </div>
              </div>
          </div>     
      

          <div class="row">
              <div class="col-md-6 col-xl-3">
                  <div class="widget-rounded-circle card-box">
                      <div class="row">
                          <div class="col-6">
                              <div class="avatar-lg rounded-circle bg-soft-primary border-primary border">
                                  <i class="fe-heart font-22 avatar-title text-primary"></i>
                              </div>
                          </div>
                          <div class="col-6">
                              <div class="text-right">
                                  <h3 class="mt-1">$<span data-plugin="counterup">58,947</span></h3>
                                  <p class="text-muted mb-1 text-truncate">Total Revenue</p>
                              </div>
                          </div>
                      </div> 
                  </div> 
              </div> 

              <div class="col-md-6 col-xl-3">
                  <div class="widget-rounded-circle card-box">
                      <div class="row">
                          <div class="col-6">
                              <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                                  <i class="fe-shopping-cart font-22 avatar-title text-success"></i>
                              </div>
                          </div>
                          <div class="col-6">
                              <div class="text-right">
                                  <h3 class="text-dark mt-1"><span data-plugin="counterup">127</span></h3>
                                  <p class="text-muted mb-1 text-truncate">Today's Sales</p>
                              </div>
                          </div>
                      </div> 
                  </div> 
              </div> 

              <div class="col-md-6 col-xl-3">
                  <div class="widget-rounded-circle card-box">
                      <div class="row">
                          <div class="col-6">
                              <div class="avatar-lg rounded-circle bg-soft-info border-info border">
                                  <i class="fe-bar-chart-line- font-22 avatar-title text-info"></i>
                              </div>
                          </div>
                          <div class="col-6">
                              <div class="text-right">
                                  <h3 class="text-dark mt-1"><span data-plugin="counterup">0.58</span>%</h3>
                                  <p class="text-muted mb-1 text-truncate">Conversion</p>
                              </div>
                          </div>
                      </div> 
                  </div> 
              </div> 

              <div class="col-md-6 col-xl-3">
                  <div class="widget-rounded-circle card-box">
                      <div class="row">
                          <div class="col-6">
                              <div class="avatar-lg rounded-circle bg-soft-warning border-warning border">
                                  <i class="fe-eye font-22 avatar-title text-warning"></i>
                              </div>
                          </div>
                          <div class="col-6">
                              <div class="text-right">
                                  <h3 class="text-dark mt-1"><span data-plugin="counterup">78.41</span>k</h3>
                                  <p class="text-muted mb-1 text-truncate">Today's Visits</p>
                              </div>
                          </div>
                      </div> 
                  </div> 
              </div> 
          </div>
      

          <div class="row">
              <div class="col-lg-4">
                  <div class="card-box">
                      <div class="dropdown float-right">
                          <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                              <i class="mdi mdi-dots-vertical"></i>
                          </a>
                          <div class="dropdown-menu dropdown-menu-right">
                             
                              <a href="javascript:void(0);" class="dropdown-item">Sales Report</a>
                             
                              <a href="javascript:void(0);" class="dropdown-item">Export Report</a>
                              
                              <a href="javascript:void(0);" class="dropdown-item">Profit</a>
                              
                              <a href="javascript:void(0);" class="dropdown-item">Action</a>
                          </div>
                      </div>

                      <h4 class="header-title mb-0">Total Revenue</h4>

                      <div class="widget-chart text-center" dir="ltr">
                          
                          <div id="total-revenue" class="mt-0"  data-colors="#f1556c"></div>

                        
                          <h2>$178</h2>

                         
                          <div class="row mt-3">
                              <div class="col-4">
                                  <p class="text-muted font-15 mb-1 text-truncate">Target</p>
                                  <h4><i class="fe-arrow-down text-danger mr-1"></i>$7.8k</h4>
                              </div>
                              <div class="col-4">
                                  <p class="text-muted font-15 mb-1 text-truncate">Last week</p>
                                  <h4><i class="fe-arrow-up text-success mr-1"></i>$1.4k</h4>
                              </div>
                              <div class="col-4">
                                  <p class="text-muted font-15 mb-1 text-truncate">Last Month</p>
                                  <h4><i class="fe-arrow-down text-danger mr-1"></i>$15k</h4>
                              </div>
                          </div>
                          
                      </div>
                  </div> 
              </div> 

              <div class="col-lg-8">
                  <div class="card-box pb-2">
                      <div class="float-right d-none d-md-inline-block">
                          <div class="btn-group mb-2">
                              <button type="button" class="btn btn-xs btn-light">Today</button>
                              <button type="button" class="btn btn-xs btn-light">Weekly</button>
                              <button type="button" class="btn btn-xs btn-secondary">Monthly</button>
                          </div>
                      </div>

                      <h4 class="header-title mb-3">Sales Analytics</h4>

                      <div dir="ltr">
                          <div id="sales-analytics" class="mt-4" data-colors="#1abc9c,#4a81d4"></div>
                      </div>
                  </div> 
              </div> 
          </div>
          

          
          
      </div> 

  </div> 

  <AppFooter/>


</div>
;
}

Dashboard.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  dashboard: makeSelectDashboard(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Dashboard);
