/**
 *
 * LoginPage
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectLoginPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import MainHeader from '../../components/MainHeader';
import 'bootstrap/dist/css/bootstrap.min.css';
import LoginLeft from '../../components/LoginLeft';
import LoginRight from '../../components/LoginRight';

export function LoginPage() {
  useInjectReducer({ key: 'loginPage', reducer });
  useInjectSaga({ key: 'loginPage', saga });

  return (
    <div>
      {/* <Helmet>
        <title>LoginPage</title>
        <meta name="description" content="Description of LoginPage" />

      </Helmet> */}
      <div>
        <div>
          <MainHeader />
        </div>

        <div style={loginLeft}>
          <LoginLeft />
        </div>

        <div style={loginRight}>
          <LoginRight />
        </div>
      </div>
    </div>
  );
}

var loginLeft = {
  width: '50%',
  float: 'left',
};

var loginRight = {
  width: '50%',
  float: 'left',
};

// var login={
//   height: "400px",
// };

LoginPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  loginPage: makeSelectLoginPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(LoginPage);
