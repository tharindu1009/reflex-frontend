/**
 *
 * TextBox
 *
 */

import React, { memo } from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

function TextBox() {
  return (
    <div>
      <div className="form-control" style={textBox} />
    </div>
  );
}

var textBox = {
  borderColor: '#007FEB',
  marginBottom: '20px',
};

TextBox.propTypes = {};

export default memo(TextBox);
