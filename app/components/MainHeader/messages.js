/*
 * MainHeader Messages
 *
 * This contains all the text for the MainHeader component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.MainHeader';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the MainHeader component!',
  },
});
