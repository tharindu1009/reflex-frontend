/**
 *
 * Asynchronously loads the component for MainHeader
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
