/**
 *
 * MainHeader
 *
 */

import React, { memo } from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import 'bootstrap/dist/css/bootstrap.min.css';

function MainHeader() {
  return (
    <div>
      <nav className="navbar row-full" style={Header}>
        <h6>ITSPTS MOBILE ADMIN</h6>
      </nav>
    </div>
  );
}

var Header = {
  backgroundColor: '#007FEB',
  width: '100%',
  color: 'white',
  fontFamily: 'Helvetica Neue',
  fontSize: '2px',
};

MainHeader.propTypes = {};

export default memo(MainHeader);
