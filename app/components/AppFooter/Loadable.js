/**
 *
 * Asynchronously loads the component for AppFooter
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
