/**
 *
 * SideNav
 *
 */

import React, { memo } from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import messages from './messages';
import 'bootstrap/dist/css/bootstrap.min.css';

function SideNav() {
  return (
    <div>
      <div style={sideNav}>
        <ul>
          <li>
            <Link>Member</Link>
          </li>
          <li>
            <Link>Packages</Link>
          </li>
          <li>
            <Link>Classes</Link>
          </li>
          <li>
            <Link>Workouts</Link>
          </li>
          <li>
            <Link>Programs</Link>
          </li>
          <li>
            <Link>Notifications</Link>
          </li>
          <li>
            <Link>My Calender</Link>
          </li>
        </ul>
      </div>
    </div>
  );
}

var sideNav = {
  color: 'white',
  textAlign: 'left',
  paddingLeft: '20px',
  backgroundColor: '#339cf5',
  width: '20%',
};

SideNav.propTypes = {};

export default memo(SideNav);
