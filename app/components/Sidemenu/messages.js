/*
 * Sidemenu Messages
 *
 * This contains all the text for the Sidemenu component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.Sidemenu';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the Sidemenu component!',
  },
});
