/**
 *
 * MainButton
 *
 */

import React, { memo } from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import 'bootstrap/dist/css/bootstrap.min.css';

function MainButton() {
  return (
    <div className="btn" style={button}>
      LOGIN
    </div>
  );
}

var button = {
  backgroundColor: '#007FEB',
  color: 'white',
};

MainButton.propTypes = {};

export default memo(MainButton);
